# Utilisez une image Python officielle en tant que base
FROM python:3.9
 
# Définissez le répertoire de travail dans le conteneur
WORKDIR /app

# Copiez le fichier requirements.txt dans le conteneur
COPY requirements.txt .

# Installez les dépendances Python
RUN pip install -r requirements.txt

# Copiez le contenu du répertoire back/ dans le conteneur
COPY . .

# Exposez le port sur lequel votre application fonctionne
EXPOSE 8000

# Commande pour démarrer l'application FastAPI avec Uvicorn
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]
