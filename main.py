import base64
from fastapi import FastAPI
from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from pydantic import BaseModel
from fastapi.staticfiles import StaticFiles


# Création d'une instance FastAPI
app = FastAPI()

class Profile(BaseModel):
    id: int
    name: str
    position: str
    imageBase64: str
    experience: str
    skills: str
    education: str

profiles = []

# Montez le répertoire statique (doit être situé dans le même répertoire que votre main.py)
app.mount("/", StaticFiles(directory=".", html=True), name="static")


# Configuration CORS pour autoriser les requêtes provenant de votre site
app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://localhost:5500", "http://localhost:8888"],  # Ajoutez votre URL frontale ici
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


# Stockage de données en mémoire
profiles = [
    {
        "id": 1,
        "name": "John Doe",
        "position": "Développeur Web",
        "imageBase64": "images/01.jpg",
        "experience": "5 ans d'expérience",
        "skills": "HTML, CSS, JavaScript, React",
        "education": "Master en informatique"
    },
    {
        "id": 2,
        "name": "Jane Smith",
        "position": "Designer UI/UX",
        "imageBase64": "images/02.jpg",
        "experience": "3 ans d'expérience",
        "skills": "Photoshop, Illustrator, Sketch, Figma",
        "education": "Licence en design graphique"
    },
    {
        "id": 3,
        "name": "Michael Johnson",
        "position": "Ingénieur Logiciel",
        "imageBase64": "images/03.jpg",
        "experience": "7 ans d'expérience",
        "skills": "Java, Python, C++, Git",
        "education": "Master en génie logiciel"
    },
    {
        "id": 4,
        "name": "Emily Brown",
        "position": "Analyste de Données",
        "imageBase64": "images/04.jpg",
        "experience": "4 ans d'expérience",
        "skills": "SQL, Python, R, Tableau",
        "education": "Master en science des données"
    },
    {
        "id": 5,
        "name": "David Williams",
        "position": "Administrateur Système",
        "imageBase64": "images/05.jpg",
        "experience": "6 ans d'expérience",
        "skills": "Linux, Bash, Docker, AWS",
        "education": "Master en informatique"
    },
    {
        "id": 6,
        "name": "Olivia Miller",
        "position": "Responsable Marketing",
        "imageBase64": "images/06.jpg",
        "experience": "5 ans d'expérience",
        "skills": "SEO, Google Analytics, Content Marketing, Social Media",
        "education": "Master en marketing"
    },
    {
        "id": 7,
        "name": "William Taylor",
        "position": "Chef de Projet",
        "imageBase64": "images/07.jpeg",
        "experience": "8 ans d'expérience",
        "skills": "Gestion de projet, Scrum, Communication, Leadership",
        "education": "Master en gestion de projet"
    },
    {
        "id": 8,
        "name": "Sophia Anderson",
        "position": "Consultante en RH",
        "imageBase64": "images/08.png",
        "experience": "6 ans d'expérience",
        "skills": "Recrutement, Formation, Gestion des performances, Droit du travail",
        "education": "Master en ressources humaines"
    },
    {
        "id": 9,
        "name": "Daniel Martinez",
        "position": "Développeur Full Stack",
        "imageBase64": "images/09.jpeg",
        "experience": "5 ans d'expérience",
        "skills": "JavaScript, Node.js, Express, MongoDB, React",
        "education": "Master en informatique"
    },
    {
        "id": 10,
        "name": "Isabella Garcia",
        "position": "Graphiste",
        "imageBase64": "images/10.jpg",
        "experience": "4 ans d'expérience",
        "skills": "Illustrator, InDesign, Photoshop, Creativité",
        "education": "Licence en design graphique"
    }
]


@app.get("/profiles/{profile_id}")
def get_profile(profile_id: int):
    for profile in profiles:
        if profile["id"] == profile_id:
            return profile
    return {"message": "Profil non trouvé"}


# Route principale avec des données de base
@app.get("/")
def read_root():
    return {"message": "Bienvenue sur mon API avec FastAPI !"}

# Route pour obtenir des informations sur une personne
@app.get("/personne/{personne_id}")
def lire_personne(personne_id: int):
    return {"personne_id": personne_id, "message": "Informations sur la personne " + str(personne_id)}

@app.post('/profiles/')
def create_profile(profile: Profile):
    profiles.append(profile.dict())
    return {'message': 'Profil créé'}

@app.put("/profiles/{profile_id}")
def update_profile(profile_id: int, profile: Profile):
    for index, p in enumerate(profiles):
        if p["id"] == profile_id:
            profiles[index] = profile.dict()
            return {"message": "Profil mis à jour"}

    raise HTTPException(status_code=404, detail="Profil non trouvé")

@app.delete("/profiles/{profile_id}")
def delete_profile(profile_id: int):
    for index, p in enumerate(profiles):
        if p["id"] == profile_id:
            deleted_profile = profiles.pop(index)
            return {"message": "Profil supprimé", "profile": deleted_profile}

    raise HTTPException(status_code=404, detail="Profil non trouvé")

@app.get("/profiles/")
def get_profiles():
    return profiles

# Exécution de l'application avec uvicorn
if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)
